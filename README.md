# LHCb DAQ Firmware Integration Testing
A repository of materials detailing how integration testing has been implemented for DAQ firmware under development for the LHCb Upgrade.

Read the integration testing documentation [here](docs/docs-home.md).

No substantive content has been added yet

## Repository Contents
- [./presentation](presentation) - Project presentation slides
- [./docs](docs) - Markdown files for longer-form documentation
  - [./docs/docs-home.md](docs/docs-home.md) - Index page for documentation

*Authored by Max Bloom as part of the Online team.*
